output "apigateway_url" {
	value = module.api.apigateway_url
}

output "website_url" {
	value = module.client.bucket_url
}
