module "api_gateway" {
	source = "terraform-aws-modules/apigateway-v2/aws"

	name                   = "order-test-apigw"
	protocol_type          = "HTTP"
	create_api_domain_name = false

	cors_configuration = {
		allow_headers = [
			"content-type"]
		allow_methods = [
			"*"]
		allow_origins = [
			"*"]
	}

	integrations = {
		"POST /orders" = {
			lambda_arn             = module.lambda.function_arn
			timeout_milliseconds   = 20000
			payload_format_version = "2.0"
		}
	}

	default_stage_access_log_destination_arn = aws_cloudwatch_log_group.apigateway_log_group.arn
	default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"
}

resource "aws_cloudwatch_log_group" "apigateway_log_group" {
	retention_in_days = 14
	name              = "order-test-apigw-log-group"
}

module "lambda" {
	source        = "git::https://github.com/tuan231195/terraform-modules.git//modules/aws-lambda?ref=master"
	function_name = "scalapay-test-function"
	handler       = "handlers/index.handler"
	source_path   = "${path.root}/../apps/api/dist"
	runtime       = "nodejs14.x"
	timeout       = 20
	environment   = {
		variables = {
			API_TOKEN       = data.aws_s3_bucket_object.api_secret.body
		}
	}
	layer_config  = {
		package_file        = "${path.root}/../apps/api/dist/package.json"
		compatible_runtimes = [
			"nodejs14.x"
		]
	}
}

data "aws_s3_bucket_object" "api_secret" {
	bucket = "vdtn359.secrets"
	key    = "scalapay-test/api-token"
}

resource "aws_lambda_permission" "apigateway_permission" {
	action        = "lambda:InvokeFunction"
	function_name = module.lambda.function_arn
	principal     = "apigateway.amazonaws.com"
	source_arn    = "${module.api_gateway.apigatewayv2_api_execution_arn}/*/*/orders"
}
