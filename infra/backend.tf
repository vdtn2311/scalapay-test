terraform {
	backend "s3" {
		encrypt = true
		bucket = "scalapay-order-test-terraform"
		key = "terraform.tfstate"
		region = "ap-southeast-2"
	}
}
