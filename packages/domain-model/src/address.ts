import { a, is } from 'yup-decorator';

export class Address {
	@is(a.string().required('Country code is required'))
	countryCode: string;

	@is(a.string().required('Line1 is required'))
	line1: string;

	@is(a.string().required('Name is required'))
	name: string;

	phoneNumber: string;

	@is(a.string().required('Postcode is required'))
	postcode: string;

	suburb: string;
}
