import { an, namedSchema, nested, nestedArray } from 'yup-decorator';
import { Address } from 'src/address';
import { Consumer } from 'src/consumer';
import { Price } from 'src/price';
import { Item } from 'src/item';
import { Merchant } from 'src/merchant';

@namedSchema('user')
export class Order {
	@nested(an.object().optional().default(undefined) as any)
	billing?: Address;

	@nested(an.object().required('Shipping address is required'))
	shipping: Address;

	@nested(an.object().required('Consumer is required'))
	consumer: Consumer;

	@nested(an.object().required('Total amount is required'))
	totalAmount: Price;

	@nested(an.object().required('Merchant is required'))
	merchant: Merchant;

	@nestedArray(
		() => Item,
		an
			.array()
			.required('Items are required')
			.min(1, 'Must have at least one item')
	)
	items: Item[];
}
