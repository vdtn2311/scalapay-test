import { a, is, nested } from 'yup-decorator';
import { Price } from 'src/price';

export class Item {
	@is(a.string().required('Category is required'))
	category: string;

	@is(a.string().required('Name is required'))
	name: string;

	@is(a.number().required('Quantity is required'))
	quantity: number;

	@is(a.string().required('SKU is required'))
	sku: string;

	@nested()
	price: Price;
}
