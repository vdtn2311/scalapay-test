import { a, is } from 'yup-decorator';

export class Consumer {
	@is(a.string().required('Email is required').email('Not a valid email'))
	email: string;

	@is(a.string().required('Given name is required'))
	givenNames: string;

	phoneNumber: string;

	@is(a.string().required('Surname is required'))
	surname: string;
}
