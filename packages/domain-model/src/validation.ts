import { getSchemaByType, validate } from 'yup-decorator';
import { Order } from 'src/order';

export const validateOrder = (object: any) => {
	return validate({
		schemaName: Order,
		object,
		options: {
			abortEarly: false,
		},
	}) as Promise<Order>;
};

export { getSchemaByType };
