import { a, is } from 'yup-decorator';

export class Merchant {
	@is(a.string().required('Redirect cancel url is required'))
	redirectCancelUrl: string;

	@is(a.string().required('Redirect confirm url is required'))
	redirectConfirmUrl: string;
}
