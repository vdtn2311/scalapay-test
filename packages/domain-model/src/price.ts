import { a, is } from 'yup-decorator';

export class Price {
	@is(
		a
			.number()
			.required('Amount is required')
			.min(0, 'Amount cannot be less than 0')
	)
	amount: number;

	@is(
		a
			.string()
			.required('Currency is required')
			.oneOf(['EUR'], 'Only EUR is supported')
	)
	currency: string;
}
