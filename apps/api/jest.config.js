const { pathsToModuleNameMapper } = require('ts-jest/utils');

const { compilerOptions } = require(`${__dirname}/tsconfig.json`);
module.exports = {
	roots: ['<rootDir>/src'],
	clearMocks: true,
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
	testURL: 'http://localhost',
	testRegex: '.*\\.(spec)\\.[tj]sx?$',
	moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, {
		prefix: `${__dirname}/`,
	}),
	globals: {
		'ts-jest': {
			compiler: 'ttypescript',
			tsconfig: `${__dirname}/tsconfig.json`,
		},
	},
	preset: 'ts-jest',
};
