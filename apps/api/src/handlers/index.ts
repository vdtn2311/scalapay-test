import { logger } from 'src/core/logger';

require('dotenv').config();

import { badRequest, error, ok } from 'src/core/response';
import { APIGatewayEvent } from 'aws-lambda';
import { validateOrder } from '@vdtn359/scalapay-domain-model';
import { CustomValidationError } from 'src/core/errors/validation-error';
import { createOrder } from 'src/services/scalapay';

export async function handler({ body: requestBody }: APIGatewayEvent) {
	try {
		if (!requestBody) {
			return badRequest('Request body cannot be empty');
		}

		const validatedOrder = await validateOrder(JSON.parse(requestBody));
		const createdOrder = await createOrder(validatedOrder);
		logger.info('Order placed successfully');

		return ok(createdOrder);
	} catch (err: any) {
		if (err.name === 'ValidationError') {
			return error(new CustomValidationError(err));
		}
		return error(err);
	}
}
