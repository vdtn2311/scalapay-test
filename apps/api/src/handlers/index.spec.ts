import { handler } from 'src/handlers/index';
import nock from 'nock';
import { config } from 'src/core/config';

process.env.API_TOKEN = 'token';

const validRequest = {
	shipping: {
		countryCode: 'au',
		line1: '3C Macarthur street',
		name: 'Fairfield',
		postcode: '2165',
	},
	consumer: {
		email: 'vdtn359@gmail.com',
		givenNames: 'Tuan',
		surname: 'Nguyen',
	},
	totalAmount: {
		amount: 123,
		currency: 'EUR',
	},
	merchant: {
		redirectCancelUrl: 'http://localhost',
		redirectConfirmUrl: 'http://localhost',
	},
	items: [
		{
			category: 'Clothes',
			name: 'Short',
			quantity: 12,
			sku: 'ABC-123',
			price: {
				amount: 123,
				currency: 'EUR',
			},
		},
	],
};

describe('End to end tests', () => {
	beforeEach(() => {
		nock.disableNetConnect();
	});
	afterEach(() => {
		nock.cleanAll();
	});

	it('should return 400 on empty body', async () => {
		expect(
			await handler({
				body: null,
			} as any)
		).toEqual({
			body: '[{"message":"Request body cannot be empty"}]',
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
			statusCode: 400,
		});
	});

	it.each(['shipping', 'consumer', 'merchant', 'totalAmount', 'items'])(
		'should return 400 on missing %s',
		async (field) => {
			const requestBody: any = { ...validRequest };
			delete requestBody[field];

			expect(
				await handler({
					body: JSON.stringify(requestBody),
				} as any)
			).toEqual(
				expect.objectContaining({
					headers: {
						'Access-Control-Allow-Origin': '*',
						'Content-Type': 'application/json',
					},
					statusCode: 400,
				})
			);
		}
	);

	it('should call to API for valid request', async () => {
		let apiRequestBody: any;
		nock(config.get('api.url'))
			.post('/v2/orders', (request) => {
				apiRequestBody = request;
				return true;
			})
			.reply(200, {
				checkoutUrl: 'http://checkoutUrl',
			});

		expect(
			await handler({
				body: JSON.stringify(validRequest),
			} as any)
		).toEqual(
			expect.objectContaining({
				body: JSON.stringify({
					checkoutUrl: 'http://checkoutUrl',
				}),
				headers: {
					'Access-Control-Allow-Origin': '*',
					'Content-Type': 'application/json',
				},
				statusCode: 200,
			})
		);

		const items = validRequest.items.map((item) => ({
			...item,
			price: { ...item.price, amount: item.price.amount.toString() },
		}));

		expect(apiRequestBody).toEqual({
			...validRequest,
			totalAmount: {
				...validRequest.totalAmount,
				amount: validRequest.totalAmount.amount.toString(),
			},
			items,
		});
	});
});
