import axios from 'axios';
import { config } from 'src/core/config';

export const client = axios.create({
	baseURL: config.get('api.url'),
	headers: {
		Authorization: `Bearer ${config.get('api.token')}`,
	},
});
