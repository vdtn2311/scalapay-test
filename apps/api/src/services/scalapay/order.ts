import { Order } from '@vdtn359/scalapay-domain-model';
import { client } from 'src/services/scalapay/client';

export const createOrder = (order: Order) => {
	const items = order.items.map((item) => ({
		...item,
		price: { ...item.price, amount: item.price.amount.toString() },
	}));

	return client
		.post('/v2/orders', {
			...order,
			totalAmount: {
				...order.totalAmount,
				amount: order.totalAmount.amount.toString(),
			},
			items,
		})
		.then(({ data }) => data);
};
