import express, { Request, Response } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { logger } from 'src/core/logger';
import { handler } from 'src/handlers';

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.post('/orders', async (req: Request, res: Response) => {
	logger.info('Creating a new order');
	const { headers, body, statusCode } = await handler({
		body: JSON.stringify(req.body),
	} as any);
	res.status(statusCode).set(headers).send(body);
});
const port = process.env.PORT || 5000;

app.listen(port, async () => {
	logger.info(`App is listening on port ${port}`);
});
