module.exports = {
	api: {
		url: 'https://staging.api.scalapay.com',
		token: process.env.API_TOKEN,
	},
};
