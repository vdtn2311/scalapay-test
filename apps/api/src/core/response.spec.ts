import { badRequest, error, response } from 'src/core/response';

describe('response', () => {
	it('should support string', () => {
		expect(response('ok', { statusCode: 200 })).toEqual({
			body: 'ok',
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'text/plain',
			},
			statusCode: 200,
		});
	});

	it('should support object', () => {
		expect(response({ message: 'ok' }, { statusCode: 200 })).toEqual({
			body: JSON.stringify({ message: 'ok' }),
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
			statusCode: 200,
		});
	});

	it('should allow setting statusCode', () => {
		expect(response({ message: 'ok' }, { statusCode: 500 })).toEqual(
			expect.objectContaining({
				statusCode: 500,
			})
		);
	});
});

describe('error', () => {
	it('should support string', () => {
		expect(error('bad')).toEqual({
			body: JSON.stringify([{ message: 'bad' }]),
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
			statusCode: 500,
		});
	});

	it('should support error object', () => {
		const errorObject = new Error('bad');
		(errorObject as any).statusCode = 400;

		expect(error(errorObject)).toEqual({
			body: JSON.stringify([{ message: 'bad' }]),
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
			statusCode: 400,
		});
	});
});

describe('badRequest', () => {
	it('should return the right response', () => {
		expect(badRequest('bad')).toEqual({
			body: JSON.stringify([{ message: 'bad' }]),
			headers: {
				'Access-Control-Allow-Origin': '*',
				'Content-Type': 'application/json',
			},
			statusCode: 400,
		});
	});
});
