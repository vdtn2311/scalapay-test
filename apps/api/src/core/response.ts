import { constants as HTTP } from 'http2';
import { logger } from 'src/core/logger';
import { CustomError } from 'src/core/errors/custom-error';

function failWithError({
	statusCode,
	message,
}: {
	statusCode: number;
	message: string | object;
}) {
	const errors =
		typeof message === 'string'
			? [
					{
						message,
					},
			  ]
			: message;
	return response(errors, {
		statusCode,
	});
}

export function error(e: string | Error) {
	if (typeof e === 'string') {
		return failWithError({
			statusCode: HTTP.HTTP_STATUS_INTERNAL_SERVER_ERROR,
			message: e,
		});
	}

	if (e instanceof CustomError) {
		return failWithError({
			statusCode: e.statusCode,
			message: e.serializeErrors(),
		});
	}

	const statusCode =
		(e as any).statusCode || HTTP.HTTP_STATUS_INTERNAL_SERVER_ERROR;
	const isInternalServerError =
		statusCode === HTTP.HTTP_STATUS_INTERNAL_SERVER_ERROR;
	if (isInternalServerError) {
		const res = (e as any).response;

		logger.error({ err: e, res }, 'Internal server error');
	}

	return failWithError({
		statusCode,
		message: isInternalServerError ? 'Internal server error' : e.message,
	});
}

export function ok(body: any, contentType = undefined) {
	return response(body, { contentType, statusCode: HTTP.HTTP_STATUS_OK });
}

export function badRequest(message: string) {
	return failWithError({ message, statusCode: 400 });
}

export function response(
	body: any,
	{ statusCode, contentType }: { statusCode: number; contentType?: string }
) {
	if (!contentType) {
		contentType =
			typeof body === 'object' ? 'application/json' : 'text/plain';
	}
	return {
		headers: {
			'Content-Type': contentType,
			'Access-Control-Allow-Origin': '*',
		},
		statusCode: statusCode,
		body:
			body && contentType === 'application/json'
				? JSON.stringify(body)
				: body,
	};
}
