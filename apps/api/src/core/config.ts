import path from 'path';

process.env.NODE_CONFIG_DIR = path.resolve(__dirname, '..', 'config');

export const config = require('config');
