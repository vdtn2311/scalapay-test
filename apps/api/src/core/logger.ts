import bunyan from 'bunyan';
import { AxiosResponse } from 'axios';

function responseSerializer(response: AxiosResponse) {
	const { config } = response;

	return {
		status: response.status,
		headers: response.headers,
		data: response.data,
		url: config.url,
		method: config.method,
	};
}

export const logger = bunyan.createLogger({
	name: 'api',
	serializers: { err: bunyan.stdSerializers.err, res: responseSerializer },
});
