export const items = [
	{
		category: 'Clothes',
		name: 'Short',
		sku: 'ABC-123',
		amount: 100,
	},
	{
		category: 'Clothes',
		name: 'T-Shirt',
		sku: 'ABC-124',
		amount: 150,
	},
	{
		category: 'Clothes',
		name: 'Socks',
		sku: 'ABC-125',
		amount: 200,
	},
];
