import { Address, Consumer } from '@vdtn359/scalapay-domain-model';

export interface Item {
	quantity: number;
	item: string;
}

export interface CustomerOrder {
	shipping: Address;
	billing?: Address;
	items: Item[];
	consumer: Consumer;
}
