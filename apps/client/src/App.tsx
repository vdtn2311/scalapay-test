import React from 'react';
import 'tailwindcss/dist/tailwind.min.css';
import { OrderForm } from './components/OrderForm';

function App() {
	return (
		<main className="w-screen min-h-screen bg-gray-200">
			<div className="container px-12 py-10">
				<OrderForm />
			</div>
		</main>
	);
}

export default App;
