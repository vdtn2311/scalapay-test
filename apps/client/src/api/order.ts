import { Order as OrderModel } from '@vdtn359/scalapay-domain-model';
import { CustomerOrder } from '../types';
import { items } from '../constant/items';

const currency = 'EUR';

const getOrder = (order: CustomerOrder): OrderModel => {
	const totalAmount = order.items.reduce((agg, current) => {
		const fullItem = items.find((item) => item.sku === current.item)!;

		return agg + current.quantity * fullItem.amount;
	}, 0);

	return {
		...order,
		totalAmount: {
			amount: totalAmount,
			currency,
		},
		items: order.items.map((currentItem) => {
			const fullItem = items.find(
				(item) => item.sku === currentItem.item
			)!;

			return {
				sku: fullItem.sku,
				quantity: currentItem.quantity,
				category: fullItem.category,
				name: fullItem.name,
				price: {
					amount: fullItem.amount,
					currency,
				},
			};
		}),
		merchant: {
			redirectCancelUrl: window.location.href,
			redirectConfirmUrl: window.location.href,
		},
	};
};

export const createOrder = async (order: CustomerOrder) => {
	const orderModel = getOrder(order);

	const response = await fetch(
		`${process.env.REACT_APP_API_URL as string}/orders`,
		{
			method: 'POST',
			headers: new Headers({
				'content-type': 'application/json',
			}),
			body: JSON.stringify(orderModel),
		}
	).then(status);

	return response.json();
};

function status(res: Response) {
	if (!res.ok) {
		throw res;
	}
	return res;
}
