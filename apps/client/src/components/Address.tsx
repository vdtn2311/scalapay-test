import { TextInput } from './common/Input';
import React from 'react';

export function Address({ addressName }: { addressName: string }) {
	return (
		<div>
			<div className="space-x-10 flex">
				<div className="w-1/2">
					<TextInput
						label={'Country code'}
						name={`${addressName}.countryCode`}
						maxLength={2}
					/>
				</div>
				<div className="w-1/2">
					<TextInput label={'Line 1'} name={`${addressName}.line1`} />
				</div>
			</div>
			<div className="space-x-10 flex">
				<div className="w-1/2">
					<TextInput label={'Name'} name={`${addressName}.name`} />
				</div>
				<div className="w-1/2">
					<TextInput
						label={'Postcode'}
						name={`${addressName}.postcode`}
					/>
				</div>
			</div>
		</div>
	);
}
