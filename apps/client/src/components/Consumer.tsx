import { TextInput } from './common/Input';
import React from 'react';

export function Consumer() {
	return (
		<div>
			<div className="space-x-10 flex">
				<div className="w-1/2">
					<TextInput
						label={'Given names'}
						name="consumer.givenNames"
					/>
				</div>
				<div className="w-1/2">
					<TextInput label={'Surname'} name="consumer.surname" />
				</div>
			</div>
			<div className="flex">
				<div className="w-1/2">
					<TextInput label={'Email'} name={'consumer.email'} />
				</div>
			</div>
		</div>
	);
}
