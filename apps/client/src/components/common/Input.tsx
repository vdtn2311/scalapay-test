import { uid } from '../../utils/id';
import React, { HTMLAttributes, useMemo } from 'react';
import { useFormContext } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

type Props = {
	name: string;
	label: string;
	type?: string;
	error?: string;
	placeholder?: string;
	maxLength?: number;
} & HTMLAttributes<HTMLInputElement>;

export function TextInput({
	label,
	error,
	type,
	placeholder,
	name,
	...props
}: Props) {
	const {
		register,
		formState: { errors },
	} = useFormContext();
	const uniqueId = useMemo(() => uid(), []);
	return (
		<div>
			<label
				className="block text-gray-700 text-sm font-bold mb-2"
				htmlFor={uniqueId}
			>
				{label}
			</label>
			<input
				{...register(name)}
				{...props}
				className={`shadow appearance-none border rounded w-full py-3 px-4 pr-8 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline ${
					error ? 'border-red-500' : ''
				}`}
				id={uniqueId}
				type={type}
				placeholder={placeholder}
			/>
			<ErrorMessage
				errors={errors}
				name={name}
				render={({ message }) => (
					<p className="text-red-500 mb-4 text-xs italic">
						{message}
					</p>
				)}
			/>
		</div>
	);
}
