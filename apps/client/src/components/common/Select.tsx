import { uid } from '../../utils/id';
import React, { useMemo } from 'react';
import { useFormContext } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

interface Option {
	label: string;
	value: string;
}

interface Props {
	label: string;
	name: string;
	options: Option[];
	error?: string;
}

export function Select({ label, error, options, name }: Props) {
	const uniqueId = useMemo(() => uid(), []);
	const {
		register,
		formState: { errors },
	} = useFormContext();

	return (
		<div>
			<label
				className="block text-gray-700 text-sm font-bold mb-2"
				htmlFor={uniqueId}
			>
				{label}
			</label>
			<select
				{...register(name)}
				className={`block appearance-none w-full border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500 ${
					error ? 'border-red-500' : ''
				}`}
			>
				{options.map((option) => (
					<option value={option.value} key={option.value}>
						{option.label}
					</option>
				))}
			</select>
			<ErrorMessage
				errors={errors}
				name={name}
				render={({ message }) => (
					<p className="text-red-500 mb-4 text-xs italic">
						{message}
					</p>
				)}
			/>
		</div>
	);
}
