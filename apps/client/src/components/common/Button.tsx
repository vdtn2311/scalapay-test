import React, { HTMLAttributes } from 'react';

type Props = {
	children: any;
	color?: string;
	type?: 'button' | 'submit' | 'reset';
	disabled?: boolean;
} & HTMLAttributes<HTMLButtonElement>;

export function Button({
	children,
	color = 'blue',
	type = 'button',
	disabled,
	...props
}: Props) {
	return (
		<button
			{...props}
			disabled={disabled}
			type={type}
			className={`h-10 px-5 my-8 text-${color}-100 bg-${color}-700 rounded-lg focus:shadow-outline hover:bg-${color}-800 ${
				disabled ? 'cursor-not-allowed opacity-70' : ''
			}`}
		>
			{children}
		</button>
	);
}
