import React, { useMemo } from 'react';
import { items } from '../constant/items';
import { Select } from './common/Select';
import { TextInput } from './common/Input';
import { Button } from './common/Button';

interface Props {
	onDelete: () => void;
	index: number;
}

export function Item({ onDelete, index }: Props) {
	const options = useMemo(
		() => items.map((item) => ({ label: item.name, value: item.sku })),
		[]
	);

	return (
		<div className="space-x-10 flex">
			<div className="w-1/3">
				<Select
					options={options}
					label="Item"
					name={`items.${index}.item`}
				/>
			</div>
			<div className="w-1/3">
				<TextInput
					label="Quantity"
					type="number"
					name={`items.${index}.quantity`}
				/>
			</div>
			<div className="w-1/3 flex items-center">
				<Button color="red" onClick={onDelete}>
					Delete
				</Button>
			</div>
		</div>
	);
}
