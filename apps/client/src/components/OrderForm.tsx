import React, { useState } from 'react';
import { Address } from './Address';
import { Consumer } from './Consumer';
import { Button } from './common/Button';
import { FormProvider, useFieldArray, useForm } from 'react-hook-form';
import { items } from '../constant/items';
import { Item } from './Item';
import * as yup from 'yup';
import {
	Address as AddressModel,
	Consumer as ConsumerModel,
	getSchemaByType,
} from '@vdtn359/scalapay-domain-model';
import { yupResolver } from '@hookform/resolvers/yup';
import { createOrder } from '../api/order';

const schema = yup.object({
	shipping: getSchemaByType(AddressModel),
	billing: getSchemaByType(AddressModel).optional().default(undefined),
	consumer: getSchemaByType(ConsumerModel),
	items: yup
		.array(
			yup.object({
				item: yup.string().required(),
				quantity: yup
					.number()
					.typeError('You must specify a number')
					.min(1, 'Must be greater than 1'),
			})
		)
		.required('Items are required')
		.min(1, 'Must have at least 1 item'),
});

export function OrderForm() {
	const [isBillingSameAsShipping, setIsBillingSameAsShipping] =
		useState(true);
	const methods = useForm({
		resolver: yupResolver(schema),
		mode: 'all',
	});
	const { control, formState, handleSubmit, setValue } = methods;
	const { isSubmitting, isValid } = formState;

	const { append, remove, fields } = useFieldArray({
		control,
		name: 'items',
	});

	const onSubmit = async (data: any) => {
		try {
			const { checkoutUrl } = await createOrder(data);
			window.location.href = checkoutUrl;
		} catch {
			alert('Failed to create order');
		}
	};

	return (
		<div className="w-full flex items-center justify-center">
			<FormProvider {...methods}>
				<form
					className="w-full md:w-2/3 p-12 bg-white rounded-lg items-center"
					onSubmit={handleSubmit(onSubmit)}
				>
					<div>
						<h6 className="text-2xl bold mb-4">Shipping address</h6>
						<Address addressName="shipping" />
					</div>
					<div className="mt-10">
						<h6 className="text-2xl bold mb-4">Billing address</h6>
						<label className="inline-flex items-center mb-4">
							<input
								type="checkbox"
								checked={isBillingSameAsShipping}
								onChange={(e) => {
									setIsBillingSameAsShipping(
										e.target.checked
									);
									if (e.target.checked) {
										setValue('billing', undefined, {
											shouldValidate: true,
										});
									}
								}}
							/>
							<span className="ml-2">
								Same as shipping address
							</span>
						</label>
						{!isBillingSameAsShipping && (
							<Address addressName="billing" />
						)}
					</div>
					<div className="mt-10">
						<h6 className="text-2xl bold mb-4">Consumer</h6>
						<Consumer />
					</div>
					<div className="mt-10">
						<h6 className="text-2xl bold mb-4">Items</h6>
						<strong>Must have at least one item</strong>
						<div className="mt-4">
							{fields.map((item, index) => (
								<Item
									key={item.id}
									onDelete={() => remove(index)}
									index={index}
								/>
							))}
						</div>
						<Button
							onClick={() =>
								append({ item: items[0].sku, quantity: 1 })
							}
						>
							Add item
						</Button>
					</div>
					<Button type={'submit'} disabled={!isValid || isSubmitting}>
						Submit
					</Button>
				</form>
			</FormProvider>
		</div>
	);
}
