### Scalapay-test

### Requirements:
* node > 12


### Demo

* Client is available at http://order-test-client-hosting-website.s3-website-ap-southeast-2.amazonaws.com
* API is available at https://0qbaxexkw1.execute-api.ap-southeast-2.amazonaws.com

### Setup
```bash
npm i
cd apps/client && npm i && cd -
cd apps/api && npm i && cd -
```

Create a .env file in apps/api with the following content
```
API_TOKEN=qhtfs87hjnc12kkos
```
### Run
```bash
npm run dev
```

* The frontend is available at http://localhost:3000
* The back end is available at http://localhost:5000

### Tests
```bash
npm run test
```

### Deployments

Requires terraform v14

```bash
cd infra
terraform init
terraform apply --auto-approve
```
